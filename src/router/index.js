import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/user/Login'
Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    name: 'Login',
    component:Login
  }
]

const router = new VueRouter({
  mode:'history',
  routes
})

export default router
